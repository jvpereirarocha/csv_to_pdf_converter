import  csv
import os

class ProcessCsvFile:
    def __init__(self, drc, filename, delimiter):
        self.__drc = drc
        self.__filename = filename
        self.__delimiter = delimiter

    @property
    def drc(self):
        return self.__drc

    @drc.setter
    def drc(self, new_drc):
        self.__drc = new_drc

    @property
    def filename(self):
      return self.__filename

    @filename.setter
    def filename(self, filename):
        self.__filename = filename

    @property
    def delimiter(self):
        return self.__delimiter

    @delimiter.setter
    def delimiter(self, delimiter):
        self.__delimiter = delimiter

    def csv_process(self):
        try:
            result = []
            with open(self.__filename, newline='') as csv_file:
                data = csv.reader(csv_file, delimiter=self.__delimiter)
                keys = None
                rows = []
                count = 0
                for row in data:
                    if count == 0:
                        keys = row
                    else:
                        d = {}
                        for x in range(0, len(row)):
                            d[keys[x]] = row[x]
                        converter = dict(d)
                        result.append(converter)
                    count += 1
            return result
        except Exception:
            raise "It was not possible process this file. Try again"
