import os
from process_file import ProcessCsvFile
from collections import namedtuple

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath('convert_pdf.py')))
CSV_FILES_DIR = os.path.join(BASE_DIR, 'files/')

file = os.path.join(CSV_FILES_DIR, 'teste.csv')

p = ProcessCsvFile(BASE_DIR, file, ',')

process = p.csv_process()


def handling_data(processed_data):
    header = []
    body = []
    first_line = True
    for data in processed_data:
        if first_line:
            header.append(list(data.keys()))
            first_line = False
        body.append(list(data.values()))
    return (header + body)


def convert_to_html(params):
    return "<html>"\
            "<head> </head>"\
            "<body>"\
                "<table>"\
                    "<thead>"\
                        "<tr>"\
                            "<th>"\
                                "{params1}"\
                            "</th>"\
                            "<th>"\
                                "{params2}"\
                            "</th>"\
                            "<th>"\
                                "{params3}"\
                            "</th>"\
                            "<th>"\
                                "{params4}"\
                            "</th>"\
                        "</tr>"\
                    "</thead>"\

                    "<tbody>"\
                        "<tr>"\
                        "<tr>"\
                    "<tbody>"\
                "</table>"\
            "</body>"\
           "</html".format(params1=params[0][0], params2=params[0][1],\
           params3=params[0][2], params4=params[0][3])


def write_file(filename):
    try:
        with open(filename, "w", newline='') as f:
            all_lines = handling_data(process)
            for data in all_lines:
                f.write("{}-{}-{}-{}"\
                .format(data[0], data[1], data[2], data[3]))
                f.write("\n")
        return True
    except Exception:
        raise "Error!"

if write_file("file.html"):
    print("Arquivo criado!")
else:
    print("Não foi possível criar o arquivo!")
